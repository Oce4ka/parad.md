// init icons
feather.replace();

// jssor slider for Outlet, Catalog

var jssorSliderOutlet_options = {
  $AutoPlay: 1,
  $AutoPlaySteps: 1,
  $HWA: false,
  $SlideDuration: 360,
  $SlideWidth: 270 * 2,
  $SlideSpacing: 60,
  $Loop: 1,
  $PauseOnHover: 3,
  $ArrowNavigatorOptions: {
    $Class: $JssorArrowNavigator$,
    $Steps: 1
  }
};

var MAX_WIDTH = 3000;

function ScaleSlider(slider) {
  var containerElement = slider.$Elmt.parentNode;
  var containerWidth = containerElement.clientWidth;
  if (containerWidth) {
    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);
    slider.$ScaleWidth(expectedWidth);
  } else {
    window.setTimeout(ScaleSlider(slider), 30);
  }
}

// Multiple sliders
$('.slider-shop').each(function () {
  console.log($(this).attr('id'));
  if (!$(this).hasClass('static')) {
    var jssorOneOfManySlider = new $JssorSlider$($(this).attr('id'), jssorSliderOutlet_options);
    /*#region responsive code begin*/
    ScaleSlider(jssorOneOfManySlider);
    $(window).bind("load", function () {
      ScaleSlider(jssorOneOfManySlider)
    });
    $(window).bind("resize", function () {
      ScaleSlider(jssorOneOfManySlider)
    });
    $(window).bind("orientationchange", function () {
      ScaleSlider(jssorOneOfManySlider)
    });
    /*#endregion responsive code end*/
  }
});

// Slider for one product item page

let productImageControl = $('.product-image-indicators').find('li').detach();
let i = 1;
for (i = 1; i <= $('.product-image-wrapper>a').length; i++) {
  $('.product-image-indicators').append(productImageControl.clone());
}
$('.product-image-indicators>li:first-child').addClass('active');
$('.product-image-indicators>li>a').click(function (e) {
  e.preventDefault();
  $('.product-image-wrapper>a').animate({'opacity': 0});
  $('.product-image-wrapper>a:nth-child(' + ($(e.target).parent('li').index() + 1) + ')').animate({'opacity': 1});
  $(e.target).parent('li').addClass('active').siblings().removeClass('active');
  console.log($(e.target).parent('li').index() + 1);
  return false;
});
$('.product-image-controls>li>a').click(function (e) {
  e.preventDefault();
  let slideQuantity = $('.product-image-wrapper>a').length;
  let slideCurrent = $('.product-image-indicators>li.active').index() + 1;
  console.log(slideCurrent, slideQuantity);
  //console.log($('.product-image-wrapper>a').length);
  if ($(e.target).parents('.product-image-controls-left').length) { //left pushed
    if (slideCurrent === 1) {
      $('.product-image-indicators>li:nth-child(' + slideQuantity + ')>a').click();
    } else {
      $('.product-image-indicators>li:nth-child(' + ((slideCurrent - 1)) + ')>a').click();
    }
  } else { //right pushed
    if (slideCurrent === slideQuantity) {
      $('.product-image-indicators>li:nth-child(1)>a').click();
    } else {
      $('.product-image-indicators>li:nth-child(' + ((slideCurrent + 1)) + ')>a').click();
    }
  }
  return false;
});

// Video
const videoSlidesCount = $(".video-js").length;

videojs("slider-video-slide-1").on('canplay', function () {
  //this.play();
  $('.video-overlay').fadeOut();
});

function resetVideoState(playNumber) {
  $('.video-js').hide();
  videojs("slider-video-slide-" + playNumber).currentTime(0);
  $('#slider-video-slide-' + playNumber).show();
  videojs("slider-video-slide-" + playNumber).play();
  $('.wrapper-slide-headers .slide-header').fadeOut();
  $('.wrapper-slide-headers .slide-header:nth-child(' + playNumber + ')').fadeIn();
  $('.menu-slides li').removeClass('active');
  $('.menu-slides li:nth-child(' + playNumber + ')').addClass('active');
}

$('.menu-slides-control').click(function (e) {
  e.preventDefault();
  //($('.menu-slides-control').index($(e.target)) + 1)
  let i;
  for (i = 1; i <= videoSlidesCount; i++) {
    videojs("slider-video-slide-" + i).pause();
  }
  let showNextSlide = $('.menu-slides-control').index($(e.target)) + 1;
  console.log(showNextSlide);
  resetVideoState(showNextSlide);
});

videojs("slider-video-slide-1").on('ended', function () {
  console.log('slider-video-slide-1 - ended');
  resetVideoState(2);
});

videojs("slider-video-slide-2").on('ended', function () {
  console.log('slider-video-slide-2 - ended');
  resetVideoState(3);
});

videojs("slider-video-slide-3").on('ended', function () {
  console.log('slider-video-slide-3 - ended');
  resetVideoState(4);
});

videojs("slider-video-slide-4").on('ended', function () {
  console.log('slider-video-slide-4 - ended');
  resetVideoState(1);
});


// show main menu
//TODO: correct hiding menu behavior

if ($('body').outerWidth() > 813) {
  if (!$('#top_navigation').hasClass('static')) {
    let timeout_hide_menu;

    function show_top_navigation() {
//    $('#top_navigation').stop();
      console.log('show_top_navigation');
      $('#top_navigation').animate({top: 0});
    }

    function hide_top_navigation() {
      console.log('show_top_navigation - timeout start');
      if (!($('.menu-shop-second:visible').length)) {
        timeout_hide_menu = setTimeout(function () {
          console.log('is animated ', $('#top_navigation').is(':animated'));
          $('.cloud-menu').fadeOut();
          $('#top_navigation').animate({top: "-6vw"});
        }, 2000);
      }
    }

    $('.main-nav').mouseenter(show_top_navigation);
    $('.main-nav').mouseenter(function () {
      clearTimeout(timeout_hide_menu);
      console.log('show_top_navigation - timeout stop');
    });
    $('#top_navigation, .main-nav').mouseleave(hide_top_navigation);

//$(window).bind("load", ScaleSlider);
//$(window).bind("resize", show_top_navigation);
    $(window).bind("scroll", show_top_navigation);
    $(window).bind("swipe", show_top_navigation);
//$(window).bind("orientationchange", ScaleSlider);
  }

  $('.fire-cloud-menu').click(function (e) {
    //e.preventDefault();
    $(this).find('.cloud-menu').fadeToggle();
    if (!$(e.target).parents('.cloud-menu').length) {
      return false;
    }
  })
} else { //For MOBILE ONLY
  $('.menu-shop-second').show();
  $('.fire-second-menu').hide();

  // Menu
  $('.menu-shop-second ul').slideUp();
  $('.menu-shop-category-wrapper>h5').click(function (e) {
    if ($(e.target).siblings('ul').is(':visible')) {
      $(e.target).siblings('ul').slideUp();
      return;
    }
    $(e.target).parents('.menu-shop-third').find('ul').slideUp();
    $(e.target).siblings('ul').slideDown();
  });
  $('.fire-second-menu').click(); //Expand Outlet
  $('.hamburger').click(function (e) {
    e.preventDefault();
    $('.menu-shop, .menu-pages').slideDown(400, function () {
    });
    $('.hamburger').hide();
    $('.hamburger-close').show();

    //$('.fire-second-menu').css({'visibility':'hidden'); //Expand Outlet
  });
  $('.hamburger-close').click(function (e) {
    e.preventDefault();
    $('.menu-shop, .menu-pages').slideUp();
    $('.hamburger-close').hide();
    $('.hamburger').show();
  });


}

//show second menu
$('.fire-second-menu').click(function (e) {
  //if is already visible
  if ($(e.target).siblings('.menu-shop-second:visible').length) {
    $(e.target).siblings('.menu-shop-second').fadeOut();
  } else {
    $('.menu-shop-second').fadeOut()
    $(e.target).siblings('.menu-shop-second').fadeIn();
  }
  return false;
});

$('body').on('click', function (e) {
  if ($('body').outerWidth() > 813) {
    if (!$(e.target).parents('#top_navigation').length) {
      $('.menu-shop-second').fadeOut();
      if ($('.main-content .menu-outlet-third').is(':visible') && $(e.target).parents('.menu-outlet').length != 1) {
        $('.main-content .menu-outlet-third').fadeOut();
      }
    }
    if (!$(e.target).parents('.menu-outlet').length && !$(e.target).parents('.main-content').length) {
      $('.main-content .menu-outlet-third').fadeOut();
    }
  }
});

//switch third menu
$('.menu-shop-third').hide();
$('.active > .menu-shop-third').show();
$('.fire-third-menu').click(function (e) {
  if ($(e.target).is('span')) {
    var targetEl = $(e.target).parent();
  } else {
    var targetEl = $(e.target);
  }
  if (!targetEl.parent().find('.menu-shop-third').is(':visible')) {
    $('.menu-shop-third').fadeOut();
    $('.menu-shop-second > li').removeClass('active');
    targetEl.parent().addClass('active');
    targetEl.parent().find('.menu-shop-third').fadeIn();
  }
  return false;
});

$('.fire-outlet-third-menu').click(function (e) {
  if ($('body').outerWidth() > 813) {
    e.preventDefault();
    if ($(e.target).is('span')) {
      var targetEl = $(e.target).parent();
    } else {
      var targetEl = $(e.target);
    }
    if (!targetEl.parent().find('.menu-outlet-third').is(':visible')) {
      $('.menu-outlet-third').fadeOut();
      $('.menu-outlet-second > li').removeClass('active');
      targetEl.parent().addClass('active');
      targetEl.parent().find('.menu-outlet-third').fadeIn();
    }
  } else {
    console.log($(e.target).parent().index());
    $('.hamburger').click();
    $('.menu-shop-second').find('li:nth-child(' + ($(e.target).parent().index() + 1) + ') a').click();
    window.scrollTo(0, 0);
  }
  return false;
});

//Modals behavior
$('.modal, .modal-close').click(function (e) {
  if ($(e.target).hasClass('modal')) {
    $(e.target).fadeOut();
  } else if ($(e.target).hasClass('modal-close') || $(e.target).hasClass('icon-x')) {
    $(e.target).parents('.modal').fadeOut();
  }
  //return false;
});

$('.showAutorizationForm, .showRegistrationForm').click(function (e) {
  var x, y;
  if ($(e.target).hasClass('showRegistrationForm') || $(e.target).parents('.showRegistrationForm').length) {
    x = 'Registration';
    y = 'Autorization';
  } else {
    x = 'Autorization';
    y = 'Registration';
  }
  $('.show' + y + 'Form > span').removeClass('chosen');
  $('.show' + x + 'Form > span').addClass('chosen');
  $('.wrapper' + y + 'Form').fadeOut();
  $('.wrapper' + x + 'Form').fadeIn();
  return false;
});

$('.fire-password').click(function (e) {
  e.preventDefault();
  $('.modal').fadeOut();
  $('.modal-password').fadeIn();
  return false;
});

$('.fire-reservation').click(function (e) {
  e.preventDefault();
  $('.modal-contact').fadeIn();
});

/*
if ($('.section-tailoring-order')) {
  $('.main-header').addClass('tailoring')
}
*/

var video = $('video').get(0);
enableInlineVideo(video);